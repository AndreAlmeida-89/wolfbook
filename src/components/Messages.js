import React, {useState, useEffect} from 'react';
import axios from 'axios';

const url = 'https://wolfbook.herokuapp.com/';



function Messages() {
    const validateBody = {
        headers:{
          "Content-Type": "application/json",
          "Authorization": localStorage.getItem('token')
        }
    }

    const [messages, setMessages] = useState([]);
    const [comment, setComment] = useState('');
    //const [id, setId] = useState();
    
        

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(url + 'posts', validateBody);
            const responseArray = response.data;
            setMessages(responseArray);
            console.log(responseArray);
        }
        fetchData();
    }, [])

    const deleteMessage = (id) =>{
        axios.delete(url + 'posts/' + id, validateBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        }) 
    }
    
    const likeMessage = (id) =>{
        const likeBody = {
            like:{
              "likable_id": id,
              "likable_type": "Post"
            }
        }
        axios.post(url + 'likes', likeBody, validateBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        }) 
    }

    const commentMessage = (id) =>{
        
        const commentBody = {
            comment:{
              "content": comment,
              "post_id": id
            }
        }
        axios.post(url + 'comments', commentBody, validateBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        })  
    }

    return (
        <div className="container"> 
            <h2>Todos os Posts:</h2> 
            {messages.map(item => (
                <div key={item.id} className="card text-white bg-info mb-3">
                    <p className="card-header">Usuário: {item.user.name}</p>
                    <h2 className="card-text">{item.content}</h2>
                    <div>
                        <button className="btn btn-primary" onClick={() => deleteMessage(item.id)}>Deletar</button>
                    </div>
                    <div>
                        <button className="btn btn-primary" onClick={() => likeMessage(item.id)}>{item.likes.length}👍</button>
                    </div>
                    <div>
                        <form onSubmit={() => commentMessage(item.id)}>
                            <input type="text" placeholder="Faça um comentário" onChange={(e)=> setComment(e.target.value)}></input>
                            <input type="submit" className="btn btn-primary"></input>
                        </form>
                    </div>
                    <div>
                        
                    </div>
                </div>
                
            ))}

               
         </div>
    )
}

export default Messages;