import React, {useState} from 'react';
import axios from 'axios';

function Login() {
    
    const url = 'https://wolfbook.herokuapp.com/';
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    

    let loginBody = {
        "user":{
            "email": email,
            "password": password
        }
    }

    const submitLogin = (e) =>{
        e.preventDefault();
        axios.post(url + 'login', loginBody)
        .then(resp => {
            console.log(resp);   
            const respToken = resp.data.token;
            localStorage.setItem('token', respToken);
            validateUser();
        })
        .catch(error => {
            console.log(error);
        })
    };

    const validateUser = () =>{
        const validateConfig = {
            headers:{
              "Content-Type": "application/json",
              "Authorization": localStorage.getItem('token')
            }
        }
        axios.get(url + 'validate_user/', validateConfig)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        })
    };
    
    
    return (
        <div className="container">
            <h2>Fazer Login:</h2>
            <form onSubmit={submitLogin}>
                <div>
                    <label>
                        E-mail
                        <input type="email" onChange={(e)=> setEmail(e.target.value)}></input>
                    </label>
                </div>

                <div>
                    <label>
                        Senha
                        <input type="password" onChange={(e)=> setPassword(e.target.value)}></input>
                    </label>
                </div>

                <div>
                    <input type="submit"></input>
                </div>
            </form>
        </div>
    )
}


export default Login