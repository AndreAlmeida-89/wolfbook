import React, {useState} from 'react';
import axios from 'axios';

function NewUser() {
    
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [gender, setGender] = useState('');
    const [password, setPassword] = useState('');
    const [passwordConfirmation, setPasswordConfirmation] = useState('');
    const [birthdate, setBirthdate] = useState('');

    let fetchBody = {
        "user":{
            "name": name,
            "email": email,
            "gender": gender,
            "password": password,
            "password_confirmation": passwordConfirmation,
            "birthdate": birthdate
        }
    }
    const submitForm = (e) =>{
        e.preventDefault();
        console.log(e);
        axios.post('https://wolfbook.herokuapp.com/sign_up', fetchBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        })
    };    
    
    return (
    <div className="container">
        <h2>Criar novo usuário:</h2>
        <form className="" onSubmit={submitForm}>
        <div>
            <label>
                Nome
                <input type="text" onChange={(e)=> setName(e.target.value)}></input>
            </label>
        </div>

        <div>
            <label>
                E-mail
                <input type="email" onChange={(e)=> setEmail(e.target.value)}></input>
            </label>
        </div>

        <div>
            <label>
                Gênero
                <input type="text" onChange={(e)=> setGender(e.target.value)}></input>
            </label>
        </div>

        <div>
            <label>
                Senha
                <input type="password" onChange={(e)=> setPassword(e.target.value)}></input>
            </label>
        </div>

        <div>
            <label>
                Confirmar
                <input type="password" onChange={(e)=> setPasswordConfirmation(e.target.value)}></input>
            </label>
        </div>

        <div>
            <label>
                Nascimento
                <input type="date" onChange={(e)=> setBirthdate(e.target.value)}></input>
            </label>
        </div>

        <div>
                <input type="submit"></input>   
        </div>

        </form>
    </div>
    );
}

export default NewUser
