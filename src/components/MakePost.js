import React, {useState} from 'react';
import axios from 'axios';

function MakePost() {
    const url = 'https://wolfbook.herokuapp.com/';
    const [post, setPost] = useState('');

    const postBody = {
        post:
            {"content":post}
    }
            
    
    const validateBody = {
        headers:{
          "Content-Type": "application/json",
          "Authorization": localStorage.getItem('token')
        }
    }

    const submitPost = (e) =>{
        e.preventDefault();
        console.log(e);
        axios.post(url + 'posts', postBody, validateBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        })  
    }
    
    return (
        <div className="container">
            <h2>Enviar Post</h2>
            <form onSubmit={submitPost}>
                <h3>Post</h3>
                <input type="text" placeholder="Escreva algo legal aqui.." onChange={(e)=> setPost(e.target.value)}></input>           
            <input type="submit"></input>
            </form>
        </div>
    )
}

export default MakePost


