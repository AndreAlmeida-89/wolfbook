import React, {useState, useEffect} from 'react';
import axios from 'axios';

function Users() {
    const url = 'https://wolfbook.herokuapp.com/';
    const [users, setUsers] = useState([]);

    const validateBody = {
        headers:{
          "Content-Type": "application/json",
          "Authorization": localStorage.getItem('token')
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(url + 'users', validateBody);
            const responseArray = response.data;
            setUsers(responseArray);
            console.log(responseArray);
        }
        fetchData();
    }, [])
    
    const requestFrienship = (id) =>{
        axios.post(url + 'friendships/' + id, {}, validateBody)
        .then(resp => {
            console.log(resp);
        })
        .catch(error => {
            console.log(error);
        })  
    }
   
    return (
        <div className="container">
            <h2>Usuários:</h2>
            
            {users.map(user => (
                <div key={user.id} className="card">
                    <h3 className="card-header">{user.name}</h3>
                    <div className="card-body">
                        <p className="card-text">Gênero: {user.gender}</p>
                        <p className="card-text">Idade: {user.age}</p>
                        <p className="card-text">E-mail: {user.email}</p>
                    </div>
                    <div>
                        <button className="btn btn-primary" onClick={() => requestFrienship(user.id)}>Solicitar Amizade</button>
                    </div>
                </div>
                    
            ))}
            
        </div>
    )
}


export default Users
