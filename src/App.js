import React from 'react';
import './App.css';
import NewUser from './components/NewUser';
import Login from './components/Login';
import Messages from './components/Messages';
import MakePost from './components/MakePost';
import Users from './components/Users'
function App() {

return (
    <div className="App">
    <h1>WolfBook</h1>
    <NewUser />
    <Login />
    <MakePost />
    <Messages />
    <Users />
    
    </div>
  );
}

export default App;
